const yaml = require('js-yaml')
const fs = require('fs')
const configFile = 'config.yml'
log4js = require('log4js')

module.exports = (() => {
  let config = {
    version: process.env.npm_package_version,
    serviceEnv: process.env.SERVICE_ENV
  }

  const trace = function(obj, ref) {
    for (let key in ref) {
      if (ref.hasOwnProperty(key)) {
        if (!obj.hasOwnProperty(key)) {
          obj[key] = ref[key]
        } else if (ref[key].constructor.name === 'Object') {
          obj[key] = trace(obj[key], ref[key])
        }
      }
    }
    return obj
  }

  try {
    Object.assign(config, yaml.safeLoad(fs.readFileSync(configFile, 'utf8')))
    let common = config.environtment.common
    if (config.environtment[config.serviceEnv]) {
      Object.assign(config, trace(config.environtment[config.serviceEnv], common))
    } else {
      Object.assign(config, common)
    }
    delete config.environtment
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e)
  }

  // config.configureLog4js = function(cat) {
  //   let parent = cat.replace(/\..*/, '')
  //
  //   let categories = {default: {appenders: ['out', 'everything'], level: this.server.logLevel.default}}
  //
  //   if (parent)
  //     categories[parent] = {
  //       appenders: ['out', 'everything'],
  //       level: this.server.logLevel[parent] || this.server.logLevel.default || 'info'
  //     }
  //
  //   if (cat)
  //     categories[cat] = {
  //       appenders: ['out', 'everything'],
  //       level: this.server.logLevel[cat] || this.server.logLevel[parent] || this.server.logLevel.default || 'info'
  //     }
  //
  //   return log4js.configure({
  //     appenders: {
  //       everything: {
  //         type: 'dateFile',
  //         filename: this.server.logDirectory + '/app.log',
  //         pattern: 'yyyyMMdd',
  //         keepFileExt: true,
  //         compress: true
  //       },
  //       out: {type: 'stdout'}
  //     },
  //     categories: categories
  //   })
  // }

  // console.log(config.server.logLevel)
  let categories = {};
  for (let cat of Object.keys(config.server.logLevel)) {
    let level = config.server.logLevel[cat]
    categories[cat] = {appenders: ['out', 'everything'], level: level}
  }
  // console.log(categories)
  log4js.configure({
    appenders: {
      everything: {
        type: 'dateFile',
        filename: config.server.logDirectory + '/app.log',
        pattern: 'yyyyMMdd',
        keepFileExt: true,
        compress: true
      },
      out: {type: 'stdout'}
    },
    categories: categories
  })

  return config
})()
