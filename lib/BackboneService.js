const transferProtocol = require('https');
const querystring = require('querystring');

// DEV
// const server = {
//     protocol: 'https',
//     host: `10.62.175.148`,
//     port: 9093,
//     path: '/api'
// };

// PROD
const server = {
    protocol: 'https',
    host: `appdev-oss.telkom.co.id`,
    port: 9093,
    path: '/api'
};

get = function(obj, key) {
    return key.split(".").reduce(function(o, x) {
        return (typeof o == "undefined" || o === null) ? o : o[x];
    }, obj);
}

function checkAPIResponseStatus(data, apiResponse) {
    let status = {
        error: false,
        code: apiResponse.statusCode,
        data: data
    };

    if (apiResponse.statusCode > 200 || apiResponse <= 300) {
        if (apiResponse.statusCode >= 400 && apiResponse.statusCode < 600) {
            status.error = true;
        }
    }

    return status;
}

function sendRequest(username, path, data, method, clientResponse, handler, errorHandler) {
    try {
        const req = transferProtocol.request({
            host: server.host,
            port: server.port,
            path: server.path + encodeURI(path) + (data && method === 'GET' ? "?" + querystring.stringify(data) : ""),
            method: method,
            "rejectUnauthorized": false,
            headers: {
                'Authorization': 'Basic bmVvNGo6dWlNWDRUTGtt',
                'Content-Type': 'application/json'
            }
        }, function (apiResponse) {
            handleResponse(apiResponse, handler, function(code, message){
                handleError(username, code, message, path, method, data, clientResponse, errorHandler);
            });
        });

        if (method !== 'GET') {
            req.write(JSON.stringify(data));
        }

        req.on('error', (e) => {
            handleError(username, 500, e.message, path, method, data, clientResponse, errorHandler);
        });

        req.end();
    }
    catch (err) {
        throw err;
    }
}

function handleResponse(apiResponse, successHandler, errorHandler) {
    let responseData = '';
    apiResponse.setEncoding('utf8');
    apiResponse.on('end', () => {
        const status = checkAPIResponseStatus(responseData, apiResponse, true);
        if (!status.error) {
            responseData = JSON.parse(status.data);
            successHandler(responseData);
        } else {
            errorHandler(status.code, status.data);
        }
    });
    apiResponse.on('data', (dataChunk) => {
        responseData += dataChunk;
    });
}

function handleError(username, status, message, path, method, data, clientResponse, errorHandler) {
    // logger.error({status: status, message: message, path: path, server: server, method: method, data: data});
    console.log({status: status, message: message, path: path, server: server, method: method, data: data});
    if (errorHandler){
        errorHandler(status, message);
    } else {
      clientResponse.status(status).send(message);
    }
}

module.exports = {
    sendRequest: sendRequest,
    server: server,
};