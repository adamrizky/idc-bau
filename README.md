# IDC Front-end UI

IDC Front-end UI is part of IDC Application bundle.  
It provides user interface for managing IDC data from
graph database.  
It requires IDC API, IDC Spatial and IDC IDC service to run.

## How to run

- Adjust configurations in `config.yml`
- For Production (SERVICE_ENV: `prod`)
  - run `npm start`
  - use below PM2 config when using PM2 for production
    ```json
    {
      "apps": [
        {
          "name": "idc-fe",
          "script": "npm",
          "args": "start",
          "env": { "NODE_ENV": "production" }
        }
      ]
    }
    ```
- For Staging (SERVICE_ENV: `stg`)
  - run `npm run stg` or
  - run `nodemon --exec npm run stg` when using `nodemon`
- For Development (SERVICE_ENV: `dev`)
  - with local debugging
    - run `npm run dev` or
    - run `nodemon --exec npm run dev` when using `nodemon`
  - with remote debugging
    - run `npm run dev-remote` or
    - run `nodemode --exec npm run dev-remote` when using `nodemon`
- Using PM2:
  - copy PM2 config template `template-pm2.json` to `pm2.json`
  - run `pm2 start pm2.json`

> **ALWAYS use pm2 in PRODUCTION !!!**
