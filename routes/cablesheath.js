const express = require('express');
const router = express.Router();
const Security = require('../lib/Security');
const auth = Security.auth;
const Backbone = require('../lib/BackboneService');
const sendRequest = Backbone.sendRequest;
const dataStructure = [
    { name: "name", label: "Name", link: "view", filter: { type: 'text' } },
    { name: "label", label: "Label", filter: { type: "text"} },
    { name: "routesection", label: "RouteSection" },
    { name: "cast_type", label: "Type", filter: { type: 'text' } },
    { name: "status", label: "Status", filter: { type: 'text', },type: 'status' },
    { name: "networkRole", label: "Network Role", filter: { type: 'text'},type:'network-role'},
    { name: "calculatedlength", label: "Calculated Length (meter)"},
    { name: "cores", label: "cores(total/used/broken)"},
    { name: "start_locn", label: "Start Locn", filter: { type: 'text' } },
    { name: "start_sto", label: "Start STO" },
    { name: "end_locn", label: "End Locn", filter: { type: 'text' } },
    { name: "end_sto", label: "End STO" },
]
router.get('/data', function (req, res, next) {
    const queries = req.query;
    var rowsx =  parseInt(queries.length);
    var pagex = (parseInt(queries.start) / rowsx) + 1;
    const requestData = {
        label: "CableSheath",
        filter: [],
        rows: rowsx,
        page: pagex
    };

    if(queries.columns) {
        queries.columns.forEach(function(item, index){
            if (item.search.value !== '' && item.name) {
                var obj = {}; 
                obj['prop'] = item.name;
                obj['value'] = item.search.value;
                requestData.filter.push(obj);
            }
        });
    }

    sendRequest('admin', '/refferenceObjects/pageQuery', requestData,  'PUT', res, function (responseData) {
        const data = [];
        var totalRows = 0;
        
        if(responseData.length > 0){
            responseData[0].nodes.forEach(function(item, index){
                var corT = item.coreInfo.total ? item.coreInfo.total:  0;
                var corU = item.coreInfo.used ? item.coreInfo.used : 0;
                var corB = item.coreInfo.broken ? item.coreInfo.broken : 0;
                data.push({
                  id: item.id ? item.id : null,
                  name: item.name ? item.name : null,
                  label: item.label ? item.label : null,
                  routesection: item.routesection ? item.routesection : null,
                  status: item.status ? item.status : null,
                  networkRole: item.networkRole ? item.networkRole : null,
                  measuredlength: item.measuredlength ? item.measuredlength : null,
                  calculatedlength: item.calculatedlength ? item.calculatedlength : null,
                  cast_type: item.type ? item.type: null,
                  cores: corT+"/"+corU+"/"+corB,
                  start_locn: item.start_locn ? item.start_locn: null,
                  start_sto: item.start_sto ? item.start_sto: null,
                  end_locn: item.end_locn ? item.end_locn: null,
                  end_sto: item.end_sto ? item.end_sto: null,
                  links: {
                    view: { url: "/cablesheath/id/" + item.id, method: "GET" }
                  }
                });
            });
            totalRows = responseData[0].pager.totalRows;
        }

        const result = {
            draw: queries.draw,
            data: data,
            recordsFiltered: totalRows,
            recordsTotal: totalRows
        };

        res.send(result);
        
    }, function(code, message) {
        const result = {
            draw: queries.draw,
            data: [],
            recordsFiltered: 0,
            recordsTotal: 0,
        }

        if(code !== 404) {
            result.error = message;
        }

        res.status(200).send(result);
    });
});

router.get('/', function (req, res, next) {
    res.render('cablesheath/list', {
        dataStructure: dataStructure,
        links: {
            data: { url: "/cablesheath/data", method: "GET" }
        }
    });
});

router.post('/', function (req, res, next) {
    auth(res, function(resp){
        const requestData ={
            name: req.body.name,
            notes: req.body.notes,
            division: req.body.division,
            status: 'INSERVICE',
            measuredlength: req.body.measuredlength,
            calculatedlength: req.body.calculatedlength,
            estimatedloss: req.body.estimatedloss,
            start_locn: req.body.start_locn,
            end_locn: req.body.end_locn,
            type: req.body.type,
            productname: req.body.productname,
            networkRole : req.body.networkrole,
            createdBy : resp
        };
        sendRequest('admin', '/cablesheath/create', requestData,  'POST', res, function (responseData) {
            res.status(200).send({
                links: {
                    view: { url: "/cablesheath/id/" + responseData.id.low, method: "GET" }
                }
            });
        });
    });
});

router.get('/:key/:value', function (req, res, next) {
    var params = "id="+req.params.value;
    if(req.params.key=="name"){
        params = "name="+Buffer.from(req.params.value, 'base64').toString('ascii');
    }
    sendRequest('admin', '/cablesheath/find?' + params , null, 'GET', res, function (responseData) {
        const data = {
            id:responseData[0].id ? responseData[0].id : null,
            name: responseData[0].name ? responseData[0].name : null,
            label: responseData[0].label ? responseData[0].label : null,
            key: responseData[0].key ? responseData[0].key : null,
            notes: responseData[0].notes ? responseData[0].notes : null,
            slack: responseData[0].slack ? responseData[0].slack : null,
            division: responseData[0].division ? responseData[0].division : null,
            start_locn: responseData[0].start_locn ? responseData[0].start_locn : null,
            end_locn: responseData[0].end_locn ? responseData[0].end_locn : null,
            measuredlength: responseData[0].measuredlength ? responseData[0].measuredlength : null,
            estimatedloss: responseData[0].estimatedloss,
            calculatedlength: responseData[0].calculatedlength ? responseData[0].calculatedlength : null,
            status: responseData[0].status,
            geometry : responseData[0].geometry,
            type : responseData[0].type ? responseData[0].type : null,
            networkRole : responseData[0].networkRole ? responseData[0].networkRole : null,
            createdAt : responseData[0].createdAt ? responseData[0].createdAt  : null,
            createdBy : responseData[0].createdBy ? responseData[0].createdBy : null,
            modifiedAt : responseData[0].modifiedAt ? responseData[0].modifiedAt : null,
            modifiedBy : responseData[0].modifiedBy ? responseData[0].modifiedBy : null,
            ownedBy : responseData[0].ownedBy ? responseData[0].ownedBy : null,
            managedBy : responseData[0].managedBy ? responseData[0].managedBy : null,
            projectId : responseData[0].projectId ? responseData[0].projectId : null,
            contractId : responseData[0].contractId ? responseData[0].contractId : null,
            cableCore:[],
            cableSheatProductType : [],
            deviceConected : [],
            device :[],
            mapName : Buffer.from(responseData[0].name).toString('base64'),
        };
        data.cableSheatProductType.push({
            name : responseData[0].cableSheatProductType.name,
            key : responseData[0].cableSheatProductType.key,
            description : responseData[0].cableSheatProductType.description,
            cores : responseData[0].cableSheatProductType.cores,
            tubes : responseData[0].cableSheatProductType.tubes,
        });
        const phyStart = {
            cql: "MATCH (d:PhysicalDevice) WHERE d.networkLocation = $name RETURN d.name as name LIMIT 20",
            params: {
              name:  responseData[0].start_locn
            }
          }
        sendRequest('admin', '/refferenceObjects/selfquery',phyStart, 'PUT', res, function (respPhyStart) {
            respPhyStart.forEach(function (item,index){
                data.device.push({
                    devicephya : item
                });
            });
            
        }, function(code, message) {
        });
        const phyEnd = {
            cql: "MATCH (d:PhysicalDevice) WHERE d.networkLocation = $name RETURN d.name as name LIMIT 20",
            params: {
              name:  responseData[0].end_locn
            }
          }

        sendRequest('admin', '/refferenceObjects/selfquery',phyEnd, 'PUT', res, function (respPhyEnd) {
            respPhyEnd.forEach(function (item,index){
                data.device.push({
                    devicephyb : item
                });
            });
        }, function(code, message) {
        });
        sendRequest('admin', '/cablesheath/findCores?name='+ data.name, null, 'GET', res, function (respCores) {
            respCores.forEach(function (item, index){
                var up = null;
                var down = null;
                item.xconnect.forEach(function (list, index){
                    if(list.direction === "UP"){
                        up=list.interface;
                    }else if(list.direction === "DOWN"){
                        down=list.interface;                        }
                });
                data.cableCore.push({
                    cablename : data.name,
                    interfaceup: up,
                    corename: item.core,
                    status: item.status,
                    interfacedown: down
                });
            });
            sendRequest('admin', '/cablesheath/deviceConnected?name='+data.name,null, 'GET', res, function (respDeviceConnected) {
                respDeviceConnected.forEach(function (item, index){
                    data.deviceConected.push({
                        locationA : item.location_A,
                        deviceA : item.device_A,
                        moduleA : item.module_A,
                        locationZ : item.location_Z,
                        deviceZ : item.device_Z,
                        moduleZ : item.module_Z
                    });  
                });
                res.render('cablesheath/details', {
                    data: data,
                    links: {
                        view: { url: "/cablesheath/id/" + data.id, method: "GET" },
                        edit: { url: "/cablesheath/" + data.id, method: "PUT" },
                        delete: {url: "/cablesheath/" + data.id, method: "DELETE"}
                    }
                });
            }, function(code, message) {
                res.render('cablesheath/details', {
                    data: data,
                    links: {
                        view: { url: "/cablesheath/id/" + data.id, method: "GET" },
                        edit: { url: "/cablesheath/" + data.id, method: "PUT" },
                        delete: {url: "/cablesheath/" + data.id, method: "DELETE"}
                    }
                });
            });
        }, function(code, message) {
            res.render('cablesheath/details', {
                data: data,
                links: {
                    view: { url: "/cablesheath/" + data.id, method: "GET" },
                    edit: { url: "/cablesheath/" + data.id, method: "PUT" },
                    delete: {url: "/cablesheath/" + data.id, method: "DELETE"}
                }
            });
        });
    });
});

router.put('/:id', function (req, res, next) {
    auth(res, function(resp){
        const id = req.params.id;
        const requestData   = [];
        var obj             = {};
        obj['id']          = id;
        obj['modifiedBy']   = resp;
        var keynames        = Object.keys(req.body);
        var x1              = req.body;
        var x2              = JSON.stringify(x1);
        var x3              = x2.split(":");
        var x4              = x3[1].split('"');
        obj[keynames]       = x4[1];
        requestData.push(obj);
        sendRequest('admin', '/cablesheath/update', requestData[0], 'PUT', res, function (resp) {
            res.status(200).send();
        });
    });
});

router.delete('/:id', function (req, res, next) {
    auth(res, function(resp){
        const id = req.params.id;
        const requestData = {
            id:id
        }
        sendRequest('admin', '/cablesheath/delete' ,requestData, 'PUT', res, function (response) {
            const result = {}
            if(response.message == 'no Action Performed'){
                result.error = 'Status cablesheath must PENDINGDELETE';
                res.status(400).send(result);
            }else{
                res.status(200).send({
                    links: {
                        index: { url: "/cablesheath/", method: "GET" }
                    }
                })
            }
        });
    });
});

router.post('/coreConnection', function (req, res, next) {
    auth(res, function(resp){
        var moduledown = req.body.module;
        var moduleup = req.body.moduleup;
        var idown = req.body.interface;
        var iup = req.body.interfaceup;
        var requestData ={
            cablesheath: {
                name: req.body.cablename,
                start_core: req.body.startcore,
                number_cores: req.body.numbercore
            },
            device_a: {
                physicaldevice: req.body.physicaldevicebname,
                module: moduleup,
                start_interface: iup
            },
            device_b: {
                physicaldevice: req.body.physicaldeviceaname,
                module: moduledown,
                start_interface: idown
            }
        };
        sendRequest('admin', '/cablesheath/createConnection', requestData,  'POST', res, function (resp) {
            res.status(200).send({
                links: {
                    view: { url: "/cablesheath/id/" + req.body.cableid, method: "GET" }
                }
            });
        });
    });
});

router.put('/:name/:core', function (req, res, next) {
    auth(res, function(resp){
        const cable = Buffer.from(req.params.name, 'base64').toString();
        const core = req.params.core;
        const requestData = {
            cableSheath : cable,
            coreNo      : core
        };
        sendRequest('admin', '/cablesheath/releaseConnection' ,requestData, 'PUT', res, function (resp) {
            res.status(200).send();
        });
    });
});

router.post('/status', function (req, res, next) {
    auth(res, function(resp){
        const requestData = {
            cablesheath: req.body.cablename,
            corelist: [
                req.body.portId
            ],
            status: req.body.status,
            modifiedBy: resp
        };
        sendRequest('admin', '/cablesheath/updateCore' ,requestData, 'PUT', res, function (responseData) {
            res.status(200).send();
        });
    });
});
module.exports = router;