const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  // {name: 'uid', label: 'Rack ID', filter: {type: 'text'}},
  {name: 'height', label: 'Height (m)', filter:{type: 'text'}},
  {name: 'width', label: 'Width (m)', filter:{type: 'text'}},
  {name: 'length', label: 'Length (m)', filter:{type: 'text'}},
  {name: 'serialNumber', label: 'Serial Number', filter:{type: 'text'}},
  {name: 'manufacturer', label: 'Manufacturer', filter:{type: 'text'}},
  {name: 'model', label: 'Model', filter:{type: 'text'}},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Rack',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          label: item.node.label ? item.node.label : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          serialNumber: item.node.serialNumber ? item.node.serialNumber : null,
          manufacturer: item.node.manufacturer ? item.node.manufacturer : null,
          model: item.node.model ? item.node.model : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/rack/' + item.node._id, method: 'GET'}
          },
          floor:[]
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('rack/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/rack/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  var type = req.body.type
  var shelf = ''
  if(type === 'FULL-RACK'){
    shelf = `WITH r
      create(s:Shelf{
        name: $name + ' - S.' + 1,
        number: 1,
        label: 'NA',
        uid: apoc.create.uuid(), 
        height: $height,
        width: $width,
        length: $length,
        serialNumber: $serialNumber,
        status: "ACTIVE",
        purpose: $purpose,
        createdAt: timestamp(),
        createdBy: "${res.locals.user}",
        createdOn: ""
      })-[:PART_OF]->(r) 
    `
  }else if(type === 'SUB-RACK'){
    shelf = `WITH r
      unwind range(1,3) as shelfId
      create(s:Shelf{
        name: $name + ' - S.' + shelfId,
        number: shelfId,
        label: 'NA',
        uid: apoc.create.uuid(), 
        height: $height,
        width: $width,
        length: $length,
        serialNumber: $serialNumber,
        status: "ACTIVE",
        purpose: $purpose,
        createdAt: timestamp(),
        createdBy: "${res.locals.user}",
        createdOn: ""
      })-[:PART_OF]->(r) 
    `
  }
  let params = req.body
  params['createdBy'] = res.locals.user
  params['spaceId'] = parseInt(req.body.spaceId)
  params['ipOn'] = req.get('host')
  params['shelfQuery'] = shelf
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_RACK',
    body : {
      params : params
    }
  }

  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/rack/' + resData[0]._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Rack',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/rack/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      name: resData.node.name ? resData.node.name : null,
      label: resData.node.label ? resData.node.label : null,
      type: resData.node.type ? resData.node.type : null,
      uid: resData.node.uid ? resData.node.uid : null,
      height: resData.node.height ? resData.node.height : null,
      width: resData.node.width ? resData.node.width : null,
      length: resData.node.length ? resData.node.length : null,
      serialNumber: resData.node.serialNumber ? resData.node.serialNumber : null,
      manufacturer: resData.node.manufacturer ? resData.node.manufacturer : null,
      model: resData.node.model ? resData.node.model : null,
      purpose: resData.node.purpose ? resData.node.purpose : null,
      status: resData.node.status ? resData.node.status : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/rack/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/rack/' + resData.node._id, method: 'PUT'}
      },
      shelfData: []
    })
    
    if(resData.relationships[0].relationship !== null){
      resData.relationships.forEach(function(item){
        if(item.node._labels.includes('Shelf')){

          var size = item.node.height + '/' + 
                     item.node.width + '/' +
                     item.node.length

          data[0].shelfData.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            number: item.node.number ? item.node.number : null,
            label: item.node.label ? item.node.label : null,
            size: size,
            purpose: item.node.purpose ? item.node.purpose : null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            createdOn: item.node.createdOn ? item.node.createdOn : null,
            links: {
              view: {url: '/shelf/' + item.node._id + '?parentId=' + resData.node._id, method : 'GET'},
              delete: {url: '/shelf/' + item.node._id + '?parentId=' + resData.node._id, method: 'DELETE'},
            }
          })
        }
      })
    }

    res.render('rack/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/rack/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
