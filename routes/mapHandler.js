const router = require('express').Router()
const internalService = require('../lib/InternalService')

router.put('/', function(req, res, next) {
  const reqSpatial = {
    wkt : req.body.geom,
    objectType : req.body.objectType,
    key : req.body.geojson.properties.uid,
    addInfo: ``
  }
  internalService.spatialAdd(reqSpatial, res, next, function(respData) {
    if(respData){
      res.json({ code: 'OK', message: req.body.objectType + ' Updated.' })
    }else{
      res.json({ code: 'FAILED', message: 'Failed Updated Spatial' })
    }
  })
})

router.delete('/', function(req, res, next) {
  const reqSpatial = {
    objectType : req.body.objectType,
    key : req.body.geojson.properties.uid
  }
  internalService.spatialDelete(reqSpatial, res, next, function(resData) {
    if(respData){
      res.json({ code: 'OK', message: req.body.objectType + ' Deleted.' })
    }else{
      res.json({ code: 'FAILED', message: 'Failed Delete Spatial' })
    }
  })
})

router.post('/', function(req, res, next) {
  const reqSpatial = {
    wkt: req.body.wkt,
    objectType : req.body.objectType,
    key: req.body.uid,
    addInfo: ``
  }
  internalService.spatialAdd(reqSpatial, res, next, function(respData) {
    if(respData){
      res.json({ code: 'OK', message: req.body.objectType + ' Created.' })
    }else{
      res.json({ code: 'FAILED', message: 'Failed add Spatial' })
    }
  })
})

module.exports = router
