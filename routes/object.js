const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'nameAlias', label: 'Name Alias', filter: {type: 'text'}},
  {name: 'description', label: 'Description', filter: {type: 'text'}},
  {name: 'status', label: 'Status', type: 'boolean'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Object',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          nameAlias: item.node.nameAlias ? item.node.nameAlias : null,
          description: item.node.description ? item.node.description : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/object/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('object/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/object/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Object',
    properties: {
      name: req.body.name,
      nameAlias: req.body.nameAlias,
      description: req.body.description,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/object/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Object',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/object/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      name: resData.node.name ? resData.node.name : null,
      nameAlias: resData.node.nameAlias ? resData.node.nameAlias : null,
      description: resData.node.description ? resData.node.description : null,
      status: resData.node.status ? resData.node.status : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/object/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/object/' + resData.node._id, method: 'PUT'},
        addObjectType: {url: '/object/addObjectType/', method: 'POST'},
      },
      objectType: [],
      objectModel: [],
      objectRole: [],
    })

    resData.relationships.forEach(function(itemRelationship){

      if(itemRelationship.relationship != null){
        

        if(itemRelationship.relationship._type == "HAS_PARENT"){

          if(itemRelationship.node._labels.includes("ObjectType")){
            data[0].objectType.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              name: itemRelationship.node.name ? itemRelationship.node.name : null,
              nameAlias: itemRelationship.node.nameAlias ? itemRelationship.node.nameAlias : null,
              description: itemRelationship.node.description ? itemRelationship.node.description : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/objectType/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/object/objectType/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }

          if(itemRelationship.node._labels.includes("ObjectModel")){
            data[0].objectModel.push({
              _id: itemRelationship.node._id ? itemRelationship.node._id : null,
              name: itemRelationship.node.name ? itemRelationship.node.name : null,
              nameAlias: itemRelationship.node.nameAlias ? itemRelationship.node.nameAlias : null,
              description: itemRelationship.node.description ? itemRelationship.node.description : null,
              status: itemRelationship.node.status ? (itemRelationship.node.status == 'ACTIVE') : null,
              createdAt: itemRelationship.node.createdAt ? itemRelationship.node.createdAt : null,
              createdBy: itemRelationship.node.createdBy ? itemRelationship.node.createdBy : null,
              updatedAt: itemRelationship.node.updatedAt ? itemRelationship.node.updatedAt : null,
              updatedBy: itemRelationship.node.updatedBy ? itemRelationship.node.updatedBy : null,
              links: {
                view: {url: '/objectModel/' + itemRelationship.node._id, method: 'DELETE'},
                delete: {url: '/object/objectModel/' + resData.node._id + '/' + itemRelationship.node._id, method: 'DELETE'},
              }
            })
          }

        }
        
      }

    })

    res.render('object/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/object/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
