const express = require('express')
const router = express.Router()

router.post('/', function(req, res, next) {
  const data = JSON.parse(req.body.value)
  res.render(req.body.tabLocation , { data : data })
})

module.exports = router
