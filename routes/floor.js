const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  {name: 'label', label: 'Label', filter: {type: 'text'}},
  {name: 'level', label: 'Level', filter: {type: 'text'}},
  {name: 'height', label: 'Height (m)', filter:{type: 'text'}},
  {name: 'width', label: 'Width (m)', filter:{type: 'text'}},
  {name: 'length', label: 'Length (m)', filter:{type: 'text'}},
  {name: 'status', label: 'Status', filter:{type: 'text'}},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Floor',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            type: item.node.type ? item.node.type : null,
            label: item.node.label ? item.node.label : null,
            level: item.node.level ? item.node.level : null,
            uid: item.node.uid ? item.node.uid : null,
            height: item.node.height ? item.node.height : null,
            width: item.node.width ? item.node.width : null,
            length: item.node.length ? item.node.length : null,
            status: item.node.status ? item.node.status: null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
            updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
            links: {
                view: {url: '/floor/' + item.node._id, method: 'GET'}
            }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('floor/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/floor/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  let params = req.body
  params['createdBy'] = res.locals.user
  params['buildingId'] = parseInt(req.body.buildingId)
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_FLOOR',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/floor/' + resData[0]._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Floor',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/floor/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
        _id: resData.node._id ? resData.node._id : null,
        name: resData.node.name ? resData.node.name : null,
        type: resData.node.type ? resData.node.type : null,
        label: resData.node.label ? resData.node.label : null,
        level: resData.node.level ? resData.node.level : null,
        uid: resData.node.uid ? resData.node.uid : null,
        height: resData.node.height ? resData.node.height : null,
        width: resData.node.width ? resData.node.width : null,
        length: resData.node.length ? resData.node.length : null,
        status: resData.node.status ? resData.node.status : null,
        createdAt: resData.node.createdAt ? resData.node.createdAt : null,
        createdBy: resData.node.createdBy ? resData.node.createdBy : null,
        updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
        updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
        links: {
            delete: {url: '/floor/' + resData.node._id + '?parentId=' + req.query.parentId, method: 'DELETE'},
            edit: {url: '/floor/' + resData.node._id, method: 'PUT'}
        },
        roomData: []
    })

    if(resData.relationships[0].relationship !== null){
      resData.relationships.forEach(function(item){
        if(item.node){
          if(item.node._labels.includes('Room')){
            data[0].roomData.push({
              _id: item.node._id ? item.node._id : null,
              type: item.node.type ? item.node.type : null,
              name: item.node.name ? item.node.name : null,
              label: item.node.label ? item.node.label : null,
              uid: item.node.uid ? item.node.uid : null,
              height: item.node.height ? item.node.height : null,
              width: item.node.width ? item.node.width : null,
              length: item.node.length ? item.node.length : null,
              squareMeter: item.node.squareMeter ? item.node.squareMeter : null,
              createdAt: item.node.createdAt ? item.node.createdAt : null,
              createdBy: item.node.createdBy ? item.node.createdBy : null,
              createdOn: item.node.createdOn ? item.node.createdOn : null,
              links: {
                view: {url: '/room/' + item.node._id + '?parentId=' + resData.node._id, method: 'GET'},
                delete: {url: '/room/' + item.node._id + '?parentId=' + resData.node._id, method: 'DELETE'},
              }
            })
          }
        }
      })
    }

    res.render('floor/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  var data = {
    nodeId : _id,
    otherNodeIds : []
  }

  data.otherNodeIds.push(req.query.parentId)

  internalService.deleteNodeWithEdges(data, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/space/',
          method: 'GET'
        }
      }
    })
  })
})

router.get('/:id/room', function(req, res, next) {
  const id = req.params.id
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Room',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [
      {
        label: "Floor",
        relationLabel: "PART_OF",
        includeRelationship: true,
        collection: true,
        propertyFilter: {
          _id: id
        }
      },
    ]
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

module.exports = router
