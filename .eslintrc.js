module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  plugins: ['pug'],
  extends: 'eslint:recommended',
  parserOptions: {
    ecmaVersion: 6
  },
  globals: {
    mainApp: true,
    serverUtils: true,
    serverConfig: true,
    log4js: true,
    $: true,
    iziToast: true,
    mapConfig: true,
    L: true,
    map: true,
    mapFunc: true,
    iziToast: true,
    Terraformer: true
  },
  rules: {
    indent: ['error', 2, {"SwitchCase": 1}],
    'max-len': ['error', {code: 160}],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    'no-multi-spaces': "error",
    // 'object-curly-spacing': ["error", "never"],
    "no-global-assign": ["error", {"exceptions": ["mainApp"]}],
    'comma-spacing': ["error", {"before": false, "after": true}],
    'computed-property-spacing': ["error", "never"],
    'space-in-parens': ["error", "never"],
    'array-bracket-spacing': ["error", "never"],

    "no-unused-vars": [2, {"args": "after-used", "argsIgnorePattern": "next|callHandler|_ignored"}]
  }
}
